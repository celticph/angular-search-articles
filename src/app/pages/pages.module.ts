import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { LayoutsModule } from '../layouts/layouts.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule, AppRoutingModule, LayoutsModule, Ng2SearchPipeModule, FormsModule
  ],
  exports: [
    HomeComponent
  ]
})
export class PagesModule { }
