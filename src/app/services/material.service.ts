import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

const material = [
  MatButtonModule,
  MatCardModule,
  MatListModule,
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatToolbarModule
]

@Injectable({
  providedIn: 'root'
})

@NgModule({
  imports: [material],
  exports: [material],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class MaterialService {

  constructor() { }
}
