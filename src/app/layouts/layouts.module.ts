import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { MaterialService } from '../services/material.service';



@NgModule({
  declarations: [
    TopNavbarComponent
  ],
  imports: [
    CommonModule, AppRoutingModule, MaterialService
  ],
  exports: [
    TopNavbarComponent
  ]
})
export class LayoutsModule { }
